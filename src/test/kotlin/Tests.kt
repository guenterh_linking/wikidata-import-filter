/*
 * wikidata-import-filter
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import java.io.File
import java.nio.charset.Charset

class Tests {
    private val log = LogManager.getLogger()
    private val props = KafkaProperties(log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties)
    private val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
    private fun readFile(fileName: String): String {
        return File("src/test/resources/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun testCase1() {
        testDriver.pipeInput(factory.create("test1", null, SbMetadataModel().setData(readFile("input1.ttl"))))
        val output =
            testDriver.readOutput("test2", StringDeserializer(), SbMetadataDeserializer())
        assertEquals("http://www.wikidata.org/entity/Q683842", output.key())
        assertEquals(readFile("output1.ttl"), output.value().data)
    }
    @Test
    fun testCase2() {
        testDriver.pipeInput(factory.create("test1", null, SbMetadataModel().setData(readFile("input2.ttl"))))
        val output =
            testDriver.readOutput("test2", StringDeserializer(), SbMetadataDeserializer())
        assert(output == null)
    }
    @Test
    fun testCase3() {
        testDriver.pipeInput(factory.create("test1", null, SbMetadataModel().setData(readFile("input3.ttl"))))
        val output =
            testDriver.readOutput("test2", StringDeserializer(), SbMetadataDeserializer())

        assertEquals("http://www.wikidata.org/entity/Q23", output.key())
        assertEquals(readFile("output3.ttl"), output.value().data)
    }
    @Test
    fun testCase4() {
        testDriver.pipeInput(factory.create("test1", null, SbMetadataModel().setData(readFile("input4.ttl"))))
        val output =
            testDriver.readOutput("test2", StringDeserializer(), SbMetadataDeserializer())

        assertEquals("http://www.wikidata.org/entity/Q5159939", output.key())
        assertEquals(readFile("output4.ttl"), output.value().data)
    }
}