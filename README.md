## Wikidata Import Filter

Filters certain messages and triples from the dataset to decrease load on the importer.

- Removes all dataset resources.
- Removes all skos:prefLabel triples.
- Removes all rdfs:label triples.